package com.atlassian.bamboo.plugins.docker;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static junitparams.JUnitParamsRunner.$;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@RunWith(JUnitParamsRunner.class)
public class RepositoryKeysTest
{
    @Test
    @Parameters
    public void testParseKey(String keyToParse, String expectedRegistry, String expectedNamespace,
                             String expectedRepository, String expectedTag)
    {
        final RepositoryKey key = RepositoryKeys.parseKey(keyToParse);
        assertThat(key.getRegistry(), equalTo(expectedRegistry));
        assertThat(key.getNamespace(), equalTo(expectedNamespace));
        assertThat(key.getRepository(), equalTo(expectedRepository));
        assertThat(key.getTag(), equalTo(expectedTag));
    }

    private Object parametersForTestParseKey()
    {
        return $(
                $("repository", null, null, "repository", "latest"),
                $("namespace/repository", null, "namespace", "repository", "latest"),
                $("repository:tag", null, null, "repository", "tag"),
                $("namespace/repository:tag", null, "namespace", "repository", "tag"),
                $("namespace/reposit/ory:t:ag", null, "namespace", "reposit/ory:t", "ag"),
                $("namespace/repository:tag/", null, "namespace", "repository", "tag/"),
                $("/repository:tag", null, "", "repository", "tag"),
                $(":tag", null, null, "", "tag"),
                $("//::", null, "", "/:", ""),
                $("registry.local:80/repository", "registry.local:80", null, "repository", "latest"),
                $("registry.local:80/namespace/repository", "registry.local:80", "namespace", "repository", "latest"),
                $("registry.local:80/context/namespace/repository", "registry.local:80", "context", "namespace/repository", "latest"),
                $("registry.local:80/repository:tag", "registry.local:80", null, "repository", "tag"),
                $("registry.local:80/namespace/repository:tag", "registry.local:80", "namespace", "repository", "tag"),
                $("registry.local/namespace/repository:tag", "registry.local", "namespace", "repository", "tag"),
                $("registry:80/namespace/repository:tag", "registry:80", "namespace", "repository", "tag"),
                $("registry/namespace/repository:tag", null, "registry", "namespace/repository", "tag")
        );
    }
}