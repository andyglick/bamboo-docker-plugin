package com.atlassian.bamboo.plugins.docker.validation;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.plugins.docker.RepositoryKey;
import com.atlassian.bamboo.plugins.docker.RepositoryKeys;
import com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.sal.api.message.I18nResolver;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import static com.atlassian.bamboo.plugins.docker.RepositoryKey.REPOSITORY_PATTERN;

public class PushConfigValidator implements ConfigValidator
{
    private static final String VALID_REGISTRY_MARKERS = ".:";

    private I18nResolver i18nResolver;

    public PushConfigValidator(I18nResolver i18nResolver)
    {
        this.i18nResolver = i18nResolver;
    }

    @Override
    public void validate(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection)
    {
        final RepositoryKey repositoryKey = RepositoryKeys.parseKey(params.getString(DockerCliTaskConfigurator.PUSH_REPOSITORY));

        if (params.getString(DockerCliTaskConfigurator.REGISTRY_OPTION).equals(DockerCliTaskConfigurator.REGISTRY_OPTION_HUB))
        {
            if (repositoryKey.getRegistry() != null)
            {
                errorCollection.addError(DockerCliTaskConfigurator.PUSH_REPOSITORY, i18nResolver.getText("docker.push.repository.registry.error.notEmpty"));
            }
        }
        else if (params.getString(DockerCliTaskConfigurator.REGISTRY_OPTION).equals(DockerCliTaskConfigurator.REGISTRY_OPTION_CUSTOM))
        {
            if (repositoryKey.getRegistry() == null)
            {
                errorCollection.addError(DockerCliTaskConfigurator.PUSH_REPOSITORY, i18nResolver.getText("docker.push.repository.registry.error.empty"));
            }
        }

        if (StringUtils.isBlank(repositoryKey.getRepository()))
        {
            errorCollection.addError(DockerCliTaskConfigurator.PUSH_REPOSITORY, i18nResolver.getText("docker.repository.error.empty"));
        }
        else if (!REPOSITORY_PATTERN.matcher(repositoryKey.getRepository()).matches())
        {
            errorCollection.addError(DockerCliTaskConfigurator.PUSH_REPOSITORY, i18nResolver.getText("docker.repository.error.invalid", repositoryKey.getRepository()));
        }

        if (StringUtils.isNotBlank(params.getString(DockerCliTaskConfigurator.USERNAME)))
        {
            if (params.getBoolean(DockerCliTaskConfigurator.CHANGE_PASSWORD) && StringUtils.isBlank(params.getString(DockerCliTaskConfigurator.PASSWORD)))
            {
                errorCollection.addError(DockerCliTaskConfigurator.PASSWORD, i18nResolver.getText("docker.password.error.empty"));
            }

            if (StringUtils.isBlank(params.getString(DockerCliTaskConfigurator.EMAIL)))
            {
                errorCollection.addError(DockerCliTaskConfigurator.EMAIL, i18nResolver.getText("docker.email.error.empty"));
            }
        }
    }
}