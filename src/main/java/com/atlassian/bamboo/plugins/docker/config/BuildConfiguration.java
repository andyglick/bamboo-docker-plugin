package com.atlassian.bamboo.plugins.docker.config;

import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator;
import com.atlassian.bamboo.task.CommonTaskContext;
import org.jetbrains.annotations.NotNull;

import java.io.File;

import static com.atlassian.bamboo.task.TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES;

public class BuildConfiguration
{
    private final String dockerfileOption;
    private final String dockerfile;
    private final String repository;
    private final boolean nocache;
    private final boolean save;
    private final String filename;
    private final String environmentVariables;
    private final File workingDirectory;

    @NotNull
    public static BuildConfiguration fromContext(@NotNull final CommonTaskContext taskContext)
    {
        return new BuildConfiguration(taskContext);
    }

    private BuildConfiguration(@NotNull final CommonTaskContext taskContext)
    {
        final ConfigurationMap configurationMap = taskContext.getConfigurationMap();

        dockerfileOption = configurationMap.get(DockerCliTaskConfigurator.DOCKERFILE_OPTION);
        dockerfile = configurationMap.get(DockerCliTaskConfigurator.DOCKERFILE);
        repository = configurationMap.get(DockerCliTaskConfigurator.REPOSITORY);
        nocache = configurationMap.getAsBoolean(DockerCliTaskConfigurator.NOCACHE);
        save = configurationMap.getAsBoolean(DockerCliTaskConfigurator.SAVE);
        filename = configurationMap.get(DockerCliTaskConfigurator.FILENAME);

        environmentVariables = configurationMap.get(CFG_ENVIRONMENT_VARIABLES);
        workingDirectory = taskContext.getWorkingDirectory();
    }

    public boolean isDockerfileInline()
    {
        return DockerCliTaskConfigurator.DOCKERFILE_OPTION_INLINE.equals(dockerfileOption);
    }

    public String getDockerfile()
    {
        return dockerfile;
    }

    public String getRepository()
    {
        return repository;
    }

    public boolean isNocache()
    {
        return nocache;
    }

    public boolean isSave()
    {
        return save;
    }

    public String getFilename()
    {
        return filename;
    }

    public String getEnvironmentVariables()
    {
        return environmentVariables;
    }

    public File getWorkingDirectory()
    {
        return workingDirectory;
    }
}