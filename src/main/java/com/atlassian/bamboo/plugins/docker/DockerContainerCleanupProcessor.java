package com.atlassian.bamboo.plugins.docker;

import com.atlassian.bamboo.build.CustomBuildProcessor;
import com.atlassian.bamboo.plugins.docker.client.Docker;
import com.atlassian.bamboo.plugins.docker.client.DockerCmd;
import com.atlassian.bamboo.plugins.docker.process.DefaultDockerProcessService;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityContext;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

/**
 * A custom build processor that cleans up Docker containers at the end of the build.
 */
public class DockerContainerCleanupProcessor implements CustomBuildProcessor
{
    @SuppressWarnings("UnusedDeclaration")
    private static final Logger log = Logger.getLogger(DockerContainerCleanupProcessor.class);
    // ------------------------------------------------------------------------------------------------------- Constants
    // ------------------------------------------------------------------------------------------------- Type Properties
    private BuildContext buildContext;

    // ---------------------------------------------------------------------------------------------------- Dependencies
    private CapabilityContext capabilityContext;

    // ---------------------------------------------------------------------------------------------------- Constructors
    // ----------------------------------------------------------------------------------------------- Interface Methods
    @Override
    public void init(@NotNull BuildContext buildContext)
    {
        this.buildContext = buildContext;
    }

    @NotNull
    @Override
    public BuildContext call() throws Exception
    {
        final String dockerPath = capabilityContext.getCapabilityValue(DockerCapabilityTypeModule.DOCKER_CAPABILITY);
        final Docker docker = new DockerCmd(dockerPath, DefaultDockerProcessService.builder().build());

        final Map<String, String> customData = buildContext.getBuildResult().getCustomBuildData();
        final Iterable<String> detachedContainers = CustomBuildDataHelper.getDetachedContainers(customData);

        for (String detachedContainer : detachedContainers)
        {
            log.debug("Removing container: " + detachedContainer);
            docker.remove(detachedContainer);
        }

        return buildContext;
    }

    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // -------------------------------------------------------------------------------------------------- Helper Methods
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
    public void setCapabilityContext(CapabilityContext capabilityContext)
    {
        this.capabilityContext = capabilityContext;
    }
}