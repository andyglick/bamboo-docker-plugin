package com.atlassian.bamboo.plugins.docker.service;

import com.atlassian.bamboo.plugins.docker.PollingService;
import com.atlassian.bamboo.plugins.docker.client.Docker;
import com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator;
import com.atlassian.bamboo.process.EnvironmentVariableAccessor;
import org.jetbrains.annotations.NotNull;

public class DockerServiceFactoryImpl implements DockerServiceFactory
{
    private final EnvironmentVariableAccessor environmentVariableAccessor;
    private final PollingService pollingService;

    public DockerServiceFactoryImpl(EnvironmentVariableAccessor environmentVariableAccessor, PollingService pollingService)
    {
        this.environmentVariableAccessor = environmentVariableAccessor;
        this.pollingService = pollingService;
    }

    @NotNull
    @Override
    public DockerService create(@NotNull final Docker docker, @NotNull final String dockerCommandOption)
    {
        if (DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_BUILD.equals(dockerCommandOption))
        {
            return new BuildService(docker);
        }

        if (DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_RUN.equals(dockerCommandOption))
        {
            return new RunService(docker, environmentVariableAccessor, pollingService);
        }

        if (DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_PUSH.equals(dockerCommandOption))
        {
            return new PushService(docker);
        }

        if (DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_PULL.equals(dockerCommandOption))
        {
            return new PullService(docker);
        }

        throw new IllegalArgumentException("No service for option " + dockerCommandOption);
    }
}
